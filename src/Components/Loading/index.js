import React from 'react'
import './index.css'

const Loading = ({ loading }) => {
  if (!loading) {
    return null
  }

  return (
    <div className="loading">
      <div className="load-container">
        <div className="lds-spinner">
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
          <div/>
        </div>
      </div>
    </div>
  )
}

export default Loading
