import React from 'react'
import { Provider } from 'mobx-react'
import {
  Redirect,
  Route,
  Router,
  Switch,
} from 'react-router'
import createBrowserHistory from 'history/createBrowserHistory'

import RootStore from '../Store'

import Home from './Home'
import SecondPage from './SecondPage'

export const history = createBrowserHistory()
const Store = new RootStore()

const App = () => {
  return (
    <Provider store={Store}>
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/second-page" component={SecondPage}/>
          <Redirect to="/"/>
        </Switch>
      </Router>
    </Provider>
  )
}

export default App;
