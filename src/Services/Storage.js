const STORAGE_KEY_AUTH = 'auth'

export const setAuthStorage = (authData) => {
    return localStorage.setItem(STORAGE_KEY_AUTH, JSON.stringify(authData))
}

export const getAuthStorage = () => {
    return JSON.parse(localStorage.getItem(STORAGE_KEY_AUTH))
}

export const clearAuthStorage = () => {
    return localStorage.removeItem(STORAGE_KEY_AUTH)
}
