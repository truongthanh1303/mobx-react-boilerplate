const CAMPAIGNS = '/campaigns';

export default {
  INDEX: '/',
  DASHBOARD: '/dashboard',
  CAMPAIGNS,
  MANAGE_ADVERTISERS: `${CAMPAIGNS}/advertisers`,
  ADS_IN_CAMPAIGN: `${CAMPAIGNS}/:campaignId/ads`,
  NEW_AD: `${this.ADS_IN_CAMPAIGN}/ad`,
  EDIT_AD: `${this.ADS_IN_CAMPAIGN}/:adId/edit/:step`,
  INVENTORY: '/inventory',
  USERS: '/users',
  REPORT: '/report',
}
