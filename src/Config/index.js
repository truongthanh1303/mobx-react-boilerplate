import { getAuthStorage } from '../Services/Storage'
import ROUTER_PATH from './RouterPath'

const authData = getAuthStorage()
const token = authData ? authData.auth_token : ''

export default {
  GLOBAL_VAR: {
    token
  },
  ROUTER_PATH,
}
